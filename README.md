Below is the R script required to produce the results presented in the journal article "Machine learning methods to assess the effects of a non-linear damage spectrum taking into account soil moisture on winter wheat yields" submitted to HESS in Germany (https://doi.org/10.5194/hess-2021-9).

You can find the scripts in order of use as well as brief descriptions and the dependencies in Overview.R .
Scripts 01 and 02 contain the scripts for spatial preprocessing of hydrometeorological data from netCDF format to tidy format (https://r4ds.had.co.nz/tidy-data.html).
The provided data (train_input_2019) are already processed with these two scripts. Therefore, the raw netCDF data are not provided. 
The yield data can be downloaded from the Federal and State Statistical Offices (https://www.regionalstatistik.de). 

The working directory to which all the references and directories are set is defined as rf-winterwheat/.

The scripts have been tested in a conda environment which is listet in conda_r-env.txt .
