# ######################
# #### Description ####
# - generate monthyl means and variance for fitting period from the daily means
# - scale explanatory data to allow clustering 
# - Apply different cluster considerations to different crops
# - scale yield data, plot this scaled yield data
# - create different data sets for clustering
# - compare data sets by hopkins statistic -> hopkins.txt (clusterability of different data sets)
# - interal validate data sets by connectivity, dunn, and sillhoutte for different cluster algorithm and different cluster sizes -> intern_plot_...
# - external validate data sets by external validation criteria -> stability (takes very long) -> stability_plot_...
# - Fit different cluster algorithm to data set choosen (n = 2:16): kmeans, pam, hierachical, density based
# - Attribute comIds to cluster (for plotting)
# - Combine attributed comIds to manually derived clusters (from Spatia_Cluster.R) -> list_cluster
#
# - Function that is applied to all crops (when data are updated to 2018 the function can just be applied to update results)
# 
# #### Input ####
# - Load Meteo and SMI Data in tidy and raw version:
#     - Meteo_train <- directory_raw, "train_input_", year_set, ".csv" <- Meteo_netcdf_to_sf.R 
# - Load data for fitting period:
#     - Yield <- directory_processed, "historical_", list_crop[crop] , start_fit, "_", end_fit ,".csv" <- BaseData_YieldDataPreProcessing.R

# 
# #### Output #### 
# ## Plots ##
# - Plot of mean and variance of standardized (scaled) yield data: 
#   directory_figures, list_crop[[crop]], "/StandardizedVarianceYield.png"
# directory_figures, list_crop[[crop]], "/StandardizedMeanYield.png"
# - intern cluster plot: directory_results, list_crop[[crop]],"_clustering_intern_plot_", names(cluster_data_list)[[d]], ".png"
# - stability cluster plot: directory_results, list_crop[[crop]],"_clustering_stability_plot_", names(cluster_data_list)[[stab]], ".png"
# 
# ## Text ##
# - Hopkins Statistic for different data.sets: directory_results, list_crop[[crop]],"_clustering_hopkins.txt"
# 
# - Export data_cluster algorithm:"
#   paste(directory_results, list_crop[[crop]], "_cluster_algorithm_",  names(cluster_data_list[d]),
#   start_fit, "_", end_fit, ".csv", sep = "")


################################################################################################################################################################################
########################################################
#### Load and pre-process data used for clustering ####
######################################################
####################################################################################################################################################################################################################################################################################################################
rm(list = ls())

source("scripts/namelist.R")


##################################
#### Load Meteo and SMI Data ####
################################
print("Load Meteo and SMI Data in tidy and raw version:")
Meteo_train <- read_csv(paste(directory_raw, "train_input_", year_set, ".csv", sep = ""))

Meteo_train

#### Check data ####
print("Distribution of the years:")

##########################################################################################################
#### Filter for years 1999 - 2018 and generate means for each comId  and split in SMI and Daily data ####
########################################################################################################
print(list_time_fit)  # 'list_time_fit defined in namelist

Meteo_train_filter <- Meteo_train %>% 
  filter(year >= list_time_fit[[1]] & year <= list_time_fit[[2]]) 

SMI <- Meteo_train_filter %>% group_by(comId) %>%  summarise(across(contains("SMI"), ~ mean(.x, na.rm = TRUE)))

Daily <- Meteo_train_filter %>% group_by(comId) %>%  summarise(across(starts_with(c("P", "T")), ~ mean(.x, na.rm = TRUE)))

print("Daily data are:")
print(Daily) # , n_extra = 10000)

print("SMI data are:")
print(SMI) #, n_extra = 10000)


######################################################################################
#### Generate monthly means and variance for fitting period from the daily means ####
####################################################################################
#### Define variables and lists used in function ####
list_meteo_months # defined in namelist.R
n_list <- matrix(nrow = 1, ncol = 96)

#################################################################################################
#### Function to generate means and variances along all columns of one month of one variable ####

print("Generate function to generate means and variances along all columsn of one month of one variable: meanVarMonths_fun")

meanVarMonths_fun <- function(x, data) {
  
  # dplyr::select all columns of one month and one variable combination
  VarsMonth_df <- data %>% dplyr::select(contains(list_meteo_months[[x]])) 
  
  # count columns in each month
  n  <- dim(VarsMonth_df)[[2]]
  
  ## Generate means for the months across all comId: need mean function to generate one value for all
  VarsMonth_mean_df <- as.data.frame(round(apply(VarsMonth_df, MARGIN = 1, mean), 2))
  VarsMonth_var_df <- as.data.frame(round(apply(VarsMonth_df, MARGIN = 1, var), 2))
  
  names(VarsMonth_mean_df) <- paste(list_meteo_months[x], "meanMonth", sep = "")
  names(VarsMonth_var_df) <- paste(list_meteo_months[x], "varMonth", sep = "")
  
  ## Bind vars and means generated ####
  VarsMonth_df <- bind_cols(VarsMonth_mean_df, VarsMonth_var_df)
  
  ## Output
  n_list[1,x] <<- n
  return(VarsMonth_df) 
}

#### Generate monthly means and vars from daily data ####
print("Apply meanVarMonths_fun to generate monthly means and variance for the Tmax, Tmin, Tavg, and P")
Monthly <- as_tibble(as.data.frame(lapply(X = c(1:48), FUN =  meanVarMonths_fun, data = Daily)))
print(Monthly)

############################################
#### Combine Monthly data with SMI Data ####
SMI_Monthly <- bind_cols(SMI, Monthly)
print("SMI_Monthly:")
print(SMI_Monthly)

#####################
#### Scale data #### 
###################

#### Scale Monthly ####
print("Scale Monthly Data")
SMI_Monthly_scaled <- SMI_Monthly %>% mutate(across(starts_with(c("SMI")), ~ scale(.x)))
print(summary(SMI_Monthly_scaled))


#### Scale Daily ####
print("Scale Daily Data")
Daily_scaled <- Daily  %>% mutate(across(starts_with(c("P", "T")), ~ scale(.x)))
print(summary(Daily_scaled))

#######################################################
#### Combine scaled data with spatial information ####
#####################################################
comId_meteo <- as_tibble(Daily$comId)
names(comId_meteo) <- "comId"
print("comId_meteo")
print(comId_meteo)

SMI_Monthly_scaled <- inner_join(comId_meteo, SMI_Monthly_scaled, by = "comId")

Daily_scaled <- inner_join(comId_meteo, Daily_scaled, by = "comId")
print(Daily_scaled)

###################################
#### Monthly without Variance ####
#################################
SMI_Monthly_scaled_Mean <- SMI_Monthly_scaled %>% dplyr::select(-contains("var"))

#####################################################################################
#### Load spatial cluster derived in Spatial_Cluster (manually derived clusters) ####
# load(file = paste(directory_results, "list_spatial.RData", sep=""))
# list_spatial_sp

####################################################################################################################################################################
####################################################################################################################################################################

#############################################
#### Create output list for cluster_func ####
list_data_cluster_algorithm <- list()

##################################################################################
#### Function that applies all cluster considerations to the different crops ####
################################################################################
print("Generate cluster_func")
  
cluster_func <- function(crop, cluster_size, cluster_data , start_fit, end_fit, hopkins_bi, cluster_clValid_intern_bi, cluster_clValid_stability_bi){
  
  ############################################
  #### Load Yield data for fitted period ####
  ##########################################
  print("The fitted period is")
  print(paste(start_fit, "to", end_fit, sep=" " ))
  
  print(paste("The crop considered for the merge with the input data is:", list_crop[[crop]]) )
  
  print("Read in data for fitting period from:")
  read_dir <- paste(directory_processed, "historical_", list_crop[crop] ,"_", start_fit, "_",end_fit ,".csv", sep = "")
  print(read_dir)
  Yield <- read_csv(read_dir)
  
  print(Yield)
  print(names(Yield))
  
  #### District information ####
  print("Districts and its number:")
  print(Yield$comId %>% unique() )
  print(length(Yield$comId %>% unique()) )# 368 (2019), #350 (2018)
  
  #########################################
  #### dplyr::select variables of need ####
  Yield <- Yield %>% dplyr::select("year", "comId", "com", "stateId", "state", "yield")
  print(Yield)
  
  ###########################
  #### Scale yield data ####
  #########################
  
  ## Generate mean and variance by comId of yield data ##
  Yield <- Yield %>% group_by(comId) %>% summarise( "yield_mean" = mean(yield, na.rm = T), "yield_var" = var(yield, na.rm = T))
  Yield %>%  summary
  
  ## Scale mean and var of Yield data ##
  print("Scale yield data")
  Yield_scaled  <- Yield  %>% mutate(across(starts_with(c("yield")), ~ scale(.x)))
  Yield_scaled %>% summary
  print(Yield_scaled %>% summary)
  
  ## Convert comId in integers ##
  krs$comId <- as.integer(krs$comId)
  Yield_scaled$comId <- as.integer(Yield_scaled$comId)
  
  ##################################################
  #### Generate spatial (sf) data for plotting ####
  ################################################
  Yield_scaled_sf <- inner_join(krs, Yield_scaled, by = "comId")
  
  #### Needs to be transfered to numerics to work in geom_sc
  Yield_scaled_sf$yield_mean <- as.numeric(Yield_scaled_sf$yield_mean)
  Yield_scaled_sf$yield_var <- as.numeric(Yield_scaled_sf$yield_var)
  
  summary(Yield_scaled_sf$yield_var )
  
  ##########################
  #### Retrieve comIds ####
  ########################
  print("Retrieve comIds for yield")
  comId <- as_tibble(Yield$comId)
  names(comId) <- "comId"
  str(comId)
  
  
  ########################################
  #### Check whether data are scaled ####
  ######################################
  print("Check scaled yield data")
  summary(Yield_scaled)
  summary(SMI_Monthly_scaled)
  summary(SMI_Monthly_scaled_Mean)
  summary(Daily_scaled[, c(1,30:40)])
  
  ######################################
  #### Create subsets to be joined ####
  ####################################
  
  ###############
  #### Yield #### 
  Yield_scaled_mean <- Yield_scaled %>% dplyr::select(-contains("var"))
    
  #######################################
  #### Monthly Variables - only Mean ####
  # Monthly_scaled <- SMI_Monthly_scaled %>% dplyr::select(-contains("SMI"))
  SMI_Monthly_scaled <- SMI_Monthly_scaled_Mean
  # SMI_Monthly_scaled_SMI_Lall <- SMI_Monthly_scaled %>% dplyr::select(-contains("SMI_L02"))
  # SMI_Monthly_scaled_SMI_L02 <- SMI_Monthly_scaled %>% dplyr::select(-contains("SMI_Lall"))
  
  #########################
  #### Daily Variables ####
  Daily_scaled
  
  
  ###############################
  #### Both SMI Lall and L02 ####
  
  ## Yield Mean ##
  YieldMeanVarsMonth  <- inner_join(Yield_scaled_mean, SMI_Monthly_scaled , by = "comId")
  YieldMeanVarsDaily  <- inner_join(Yield_scaled_mean, Daily_scaled,   by = "comId")
  YieldMeanVarsAll    <- inner_join(YieldMeanVarsDaily,  SMI_Monthly_scaled ,   by = "comId")
  YieldMeanVarsAll_noMonthlyAvg <- YieldMeanVarsAll %>% dplyr::select(-contains("months"))
  YieldMeanVarsAll_noSMI <- YieldMeanVarsAll %>% dplyr::select(-contains("SMI"))
  names( YieldMeanVarsAll_noSMI)

  # Yield Mean & Var
  YieldMeanVarVarsMonth   <- inner_join(Yield_scaled, SMI_Monthly_scaled , by = "comId")
  YieldMeanVarVarsDaily   <- inner_join(Yield_scaled, Daily_scaled,   by = "comId")
  YieldMeanVarVarsAll     <- inner_join(YieldMeanVarVarsDaily,  SMI_Monthly_scaled ,   by = "comId")

  ######################
  #### Only SMI_all ####
  
  ## Yield Mean ##
  YieldMeanVarsMonth_SMIall <- YieldMeanVarsMonth   %>% dplyr::select(-contains("SMI_L02"))
  YieldMeanVarsDaily_SMIall <- YieldMeanVarsDaily   %>% dplyr::select(-contains("SMI_L02"))
  YieldMeanVarsAll_SMIall   <- YieldMeanVarsAll     %>% dplyr::select(-contains("SMI_L02"))
  YieldMeanVarsMonth_SMIall %>% dplyr::select(contains("SMI_Lall"))

  ## Yield Mean & Var ##
  YieldMeanVarVarsMonth_SMIall  <- YieldMeanVarVarsMonth  %>% dplyr::select(-contains("SMI_L02"))
  YieldMeanVarVarsDaily_SMIall  <- YieldMeanVarVarsDaily  %>% dplyr::select(-contains("SMI_L02"))
  YieldMeanVarVarsAll_SMIall    <- YieldMeanVarVarsAll    %>% dplyr::select(-contains("SMI_L02"))

  ######################
  #### Only SMI_L02 ####
  
  # ## Yield Mean ##
  YieldMeanVarsMonth_SMIL02 <- YieldMeanVarsMonth   %>% dplyr::select(-contains("SMI_Lall"))
  YieldMeanVarsDaily_SMIL02 <- YieldMeanVarsDaily   %>% dplyr::select(-contains("SMI_Lall"))
  YieldMeanVarsAll_SMIL02   <- YieldMeanVarsAll     %>% dplyr::select(-contains("SMI_Lall"))
  YieldMeanVarsMonth_SMIL02 %>% dplyr::select(contains("SMI_L02"))

  ## Yield Mean & Var ##
  YieldMeanVarVarsMonth_SMIL02  <- YieldMeanVarVarsMonth  %>% dplyr::select(-contains("SMI_Lall"))
  YieldMeanVarVarsDaily_SMIL02  <- YieldMeanVarVarsDaily  %>% dplyr::select(-contains("SMI_Lall"))
  YieldMeanVarVarsAll_SMIL02    <- YieldMeanVarVarsAll    %>% dplyr::select(-contains("SMI_Lall"))
  YieldMeanVarVarsAll_SMIL02 %>% dplyr::select(contains("SMI_L02"))

  #### Generate List including all data.frames ####
  cluster_data_list_all <- list(
                       "YieldMeanVarsMonth_SMI_L02all" = YieldMeanVarsMonth,
                       "YieldMeanVarsDaily_SMI_L02all" = YieldMeanVarsDaily,
                       "YieldMeanVarsAll_SMI_L02all" = YieldMeanVarsAll,
                       "YieldMeanVarVarsMonth_SMI_L02all" = YieldMeanVarVarsMonth,
                       "YieldMeanVarVarsDaily_SMI_L02all" = YieldMeanVarVarsDaily,
                       "YieldMeanVarVarsAll_SMI_L02all" = YieldMeanVarVarsAll,

                       "YieldMeanVarsMonth_SMIall" = YieldMeanVarsMonth_SMIall,
                       "YieldMeanVarsDaily_SMIall" = YieldMeanVarsDaily_SMIall,
                       "YieldMeanVarsAll_SMIall" = YieldMeanVarsAll_SMIall,
                       "YieldMeanVarVarsMonth_SMIall" = YieldMeanVarVarsMonth_SMIall,
                       "YieldMeanVarVarsDaily_SMIall" = YieldMeanVarVarsDaily_SMIall,
                       "YieldMeanVarVarsAll_SMIall" = YieldMeanVarVarsAll_SMIall,
                       #
                       "YieldMeanVarsMonth_SMIL02" = YieldMeanVarsMonth_SMIL02,
                       "YieldMeanVarsDaily_SMIL02" = YieldMeanVarsDaily_SMIL02,
                       "YieldMeanVarsAll_SMIL02" = YieldMeanVarsAll_SMIL02,
                       "YieldMeanVarVarsMonth_SMIL02" = YieldMeanVarVarsMonth_SMIL02,
                       "YieldMeanVarVarsDaily_SMIL02" = YieldMeanVarVarsDaily_SMIL02,
                       "YieldMeanVarVarsAll_SMIL02" = YieldMeanVarVarsAll_SMIL02
  )
  

  ##############################################
  #### Choose data set used for clustering ####
  ############################################
  print("The data for clustering that are considered are:")
  cluster_data_list <- cluster_data_list_all[cluster_data]
  names(cluster_data_list)
  
  str(cluster_data_list,1)
  # cluster_data_list[[1]]
  print("List of various data configuration for clustering considered:")
  print(str(cluster_data_list, 1))

  
  ######################################################################
  #### Loop to write out cluster statistics for different data sets ####
  #####################################################################

  for (d in seq_along(cluster_data_list)) {
    
    ##############################
    #### Data for clustering ####
    ############################
    print("Data used for clustering are:")
    print(names(cluster_data_list)[[d]])
    data <-  cluster_data_list[[d]]  %>% dplyr::select(-contains("comId"))
    print((data))
    
    #### Function to give out hopkins ####
    if (hopkins_bi) {

      ## As various combinations of data are possible the following function
      print("Generate cluster_hopkins - , cluster_clValid_intern - , and cluster_clValid_stability - function")


      #######################################################################################
      #### Test for clustering tendency -  i.e. whether data are suitable for clustering ####
      print("Start hopkins fuction")
      nrow_value <- nrow(data)-1
      print(nrow_value)
      hopkins <- hopkins(data, n = nrow_value)
      names(hopkins) <- names(cluster_data_list)[[d]]

      #### Export results ####
      print("Export Hopkins:")
      print(hopkins)
      hopkins_path <-  paste(directory_results, list_crop[[crop]],"_clustering_hopkins_", names(cluster_data_list)[[d]],".txt", sep = "")
      print(hopkins_path)

      capture.output(hopkins, file =, append = TRUE)


      # capture.output(hopkins, file = paste("results/clustering/hopkins_", names(cluster_data_list)[[x]], ".txt", sep = ""))

      # get_clust_tendency(data, n = 50,
      #                    gradient = list(low = "steelblue",  high = "white")) # hopkins is 0.16 -> good for clustering.
      # 'the smaller hopkins stat the better suited for clustering. 0.5 indicate high degree of spatial randomness.'


    } else {
      print("No hopkins statistic")
    } # hopkins_bi

    ######################################
    #### Function to give out hopkins ####
    ######################################
    if (cluster_clValid_intern_bi) {

      #########################################
      #### Define data used for clustering ####

      rownames(data_matrix)
      data_matrix <- as.matrix(data)

      ###################################################################
      #### Choose best algorithm and number of clusters for the data ####
      'clValid: comparing multiple clustering algorithms in a single function call for identifying the best clustering approach and the optimal number of cluster'

      ## Compare clustering algorithms
      set.seed(123)
      cluster_intern <- clValid(data_matrix, nClust = cluster_size,
                                 clMethods = c("hierarchical", "kmeans", "pam"),
                                 validation = "internal", verbose = T)
      # fanny and som algortihm is not working, model takes too long ("model"), agnes exact same results compared to hierarchical
      # taken out for publication  "diana", "sota",, "clara"
      str(cluster_intern, 2)

      print(summary(cluster_intern ))


      #################################################
      #### Export results from internal validation ####
      path_cluster_intern <- paste(directory_results, list_crop[[crop]],"_clustering_intern_",
                                   names(cluster_data_list)[d],".txt", sep = "")

      capture.output(summary(cluster_intern ) , file =  path_cluster_intern )

      ## Plot results
      png(paste(directory_results, list_crop[[crop]],"_clustering_intern_plot_", names(cluster_data_list)[[d]], ".png", sep = ""),
          width = 400, height = 600)
      par(mfrow = c(3,1))
      plot(cluster_intern)
      dev.off()

      ################################################################################################
      #### Plot cluster statistics of Connectivity, Dunn Index, and Silhoutte Width with ggplot2 ####
      ##############################################################################################

      #######################
      #### Prepare data  ####

      #### Retrieve internal data measures from cluster_intern class ####
      internal_data <- cluster_intern@measures %>% as.data.frame()

      # #### Change names ####
      # names(internal_data)  <- c(paste("hierachical", cluster_size, sep="_") ,  paste("kmeans", cluster_size, sep="_") ,  paste("pam", cluster_size, sep="_"))

      internal_data

      #### Pivot to long data frame ####
      internal_data_longer <- internal_data %>% pivot_longer( cols = 1:last_col())

      print(internal_data_longer , n=150)

      #### Add column that names metric ####
      names_innerMetrics <- c(rep("Connectivity", 3*length(cluster_size)) , rep("Dunn Index", 3*length(cluster_size)), rep("Silhouette Width", 3*length(cluster_size))) %>% enframe(name = NULL)

      internal_data_longer <-  bind_cols(internal_data_longer, names_innerMetrics)

      #### Seperate first column into size and ClusterAlgorithm ####

      internal_data_longer <- internal_data_longer %>% separate(name, c("size", "clusterAlgorithm"))

      #### Change names ####
      names(internal_data_longer) <- c( "size","clusterAlgorithm","values", "internalMeasure")

      #### Convert character column of size to numerics to allow use in ggplot2 ####
      internal_data_longer$size <- as.double(internal_data_longer$size)


      print( internal_data_longer,  n=150)


      ######################################
      #### Generate plots of statistics ####

      internal_plot <- ggplot(internal_data_longer, aes(size, values, color = clusterAlgorithm)) +
        facet_wrap(~internalMeasure, scales = "free")  +
        geom_line(size = 2)    +
        labs(color = "Cluster algorithm") +
        scale_color_manual(values = wes_palette("Royal1", n = 3))  +
        theme_minimal() +
        theme(axis.ticks.x = element_line(size = 4), axis.text.x = element_text(size = 20, color = "black") , axis.title.x = element_text(size = 20, color = "black") ) +
        theme(axis.ticks.y = element_line(size = 4), axis.text.y = element_text(size = 20, color = "black") , axis.title.y = element_blank() ) +
        theme(strip.text = element_text(size = 20, color = "black") ) +
        theme(legend.text = element_text(size = 20, color = "black") , legend.title = element_text(size = 20, color = "black") )

      ###########################################
      #### Export plot of cluster statistics ####

      print("Export plot of internal cluster statistics:")
      internal_plot_path <- paste(directory_results, list_crop[[crop]],"_clustering_intern_plot_ggplot2_", names(cluster_data_list)[[d]], ".png", sep = "")
      print(internal_plot_path )

      ggsave(plot = internal_plot, filename = internal_plot_path ,
             width = 16, height = 8)
    } else {
      print("No cluster_clValid_intern")
      } # cluster_clValid_intern_bi

    #########################################################################
    #### Function to give out statisitcs on validity cluster comparision ####
    #########################################################################
     if (cluster_clValid_stability_bi)
    {

      ####################################################################
      #### Choose best algorithm and number of clusters for the data ####
      ##################################################################
      # 'clValid: comparing multiple clustering algorithms in a single function call for identifying the best clustering approach and the optimal number of cluster'

      ##
      set.seed(123)
      #### Test in accordance to stability measures
      stability <- clValid (data, nClust = cluster_size,
                            clMethods = c("hierarchical"),
                            validation = "stability", verbose = T)

      capture.output(summary(stability), file = paste(directory_results, list_crop[[crop]],"_clustering_stability_",
                                                      names(cluster_data_list)[[stab]], ".txt", sep = ""))
      png(paste(directory_results, list_crop[[crop]],"_clustering_stability_plot_", names(cluster_data_list)[[stab]], ".png", sep = ""),
          width = 1800, height = 900)
      # 2. Create a plot
      par(mfrow = c(1,4))
      plot(stability)
      # Close the pdf file
      dev.off()
      #
    }  else {
    print("No cluster_clValid_stability")
    } # cluster_clValid_stability_bi

    ##########################################################################################################################
    #### CLuster Algorithm Approach: Use different algorithm to generate Cluster and attribute this cluster to comIds    ####
    ########################################################################################################################

    ######################################################
    #### Fit clustering Algorithms for cluster n < 16 ####
    print("Start to fit different cluster for size n < 16")
    
    print("The cluster sizes defined are:")
    print(cluster_size)
    
    ################    
    #### kmeans ####
    print("kmeans")
    kmeans_fun <- function(x){
      kmeans_cl <- kmeans(data, x)
      kmeans_cl$cluster
    }
    
    kmeans_list <- lapply(cluster_size, kmeans_fun)
    kmeans_list
    kmeans_data <- as_tibble(as.data.frame(kmeans_list))
    names(kmeans_data) <- paste("kmeans", cluster_size, sep="")
    
    #############
    #### pam ####
    print("pam")
    pam_fun <- function(x){
      pam_cl <- pam(data, x)
      pam_cl$clustering
    }
    
    pam_list <- lapply(cluster_size, pam_fun)
    pam_list
    
    pam_data <- as_tibble(as.data.frame(pam_list))
    names(pam_data) <- paste("pam", cluster_size, sep="")
    
    ######################
    #### hierarchical ####
    print("hierarchical")
    res.hc <- hclust(dist(data, method = "euclidean"), method = "ward.D2" )
    
    # grp <- cutree(res.hc, k = n)
    # grp
    
    hc_fun <- function(x) {grp <- cutree(res.hc, k = x)}
    hc_list <-  lapply(cluster_size, hc_fun)
    
    hc_data <- as_tibble(as.data.frame(hc_list))
    names(hc_data) <- paste("hc", cluster_size, sep="")
    # ##################################
    # #### density based clustering ####
    
    # !!! dbscan doesnt work with data without cOmId
    
    # print("density based")
    # # kNNdistplot(data, k = 10)
    # # abline(h=80, col = "red", lty=2)
    # 
    # dbscan_list_2 <- list(c(10,1),c(15,250),c(30,500),c(45,750))
    # 
    # dbscan_fun <- function(x){
    #   res <- dbscan(data, eps = dbscan_list_2[[x]][2], minPts = dbscan_list_2[[x]][1])
    #   res$cluster
    # }
    # 
    # dbscan_list <- lapply(1:4, dbscan_fun)
    # dbscan_data <- as_tibble(as.data.frame(dbscan_list))
    # names(dbscan_data) <- paste("dbscan", c(1:4), sep="")
    # 
    
    #######################################################################################
    #### Generate data frame containing data derived from different cluster algorithm ####
    data_cluster_algorithm <- as_tibble(bind_cols(comId, kmeans_data, pam_data, hc_data))

    ########################################
    #### Export dataframe  of clusters ####
    ######################################
    print("Export data_cluster algorithm:")
    export_file2 <- paste(directory_results,  "data_cluster_algorithm_", list_crop[[crop]], "_", names(cluster_data_list[d]),
                          start_fit, "_", end_fit, ".csv", sep = "")
    print(export_file2)
    write_csv(data_cluster_algorithm , file = export_file2 )

  } # End of loop through cluster_data_list
  
}# End of function

########################################################################################
#### Print list of clustering data available in this script but stored in namelist.R ####
print(cluster_data_list_names)
print(list_crop[[1]])

####################################################
#### Apply cluster_func through different crops ####
cluster_func( #FUN = cluster_func, 
       crop = 1, # list_crop
       cluster_size = c(2:16), 
       cluster_data = cluster_data_list_names[c(1,3)], # "YieldMeanVarsAll_SMI_L02all", 
       start_fit = list_time_fit[[1]], # 1999 
       end_fit = list_time_fit[[2]] , # 2018
       hopkins_bi = F, 
       cluster_clValid_intern_bi = F,
       cluster_clValid_stability_bi = F
       )
