#### Define loyo_function
loyo_function <- function(loyo){
  
  #### Filter to leave one year out ####
  print(paste("Keep out", list_years_train[[loyo]] ))
  data_train_spat_time_loyo <- data_train_spat_time %>% filter(year != list_years_train[[loyo]])
  print("The years in the leave-one-year-out dataset are:")
  print(unique(data_train_spat_time_loyo$year))
  
  #############################################################
  #### Data.frame including variables used in RandomForest ####
  data_train_spat_time_loyo_rf <- data_train_spat_time_loyo %>% select(-c("year", "comId", "com", "stateId", "state", "cluster"))
  
  ###################################################################################################################
  #### Run Random Forests for the comIds derived for a particular cluster size of a particular cluster algorithm ####
  ###################################################################################################################
  print(paste("Train Model with ntree = ", ntree))
  
  ## Define yield data depending on indepdendent_variable ##
  yield <- data_train_spat_time_loyo_rf %>% pull(independent_variable)
  set.seed(1)
  modelRF_loyo <- tuneRF(x = data_train_spat_time_loyo_rf[,-1], y = yield ,  ntree =  ntree,  doBest = T, plot = TRUE)
  
  print(modelRF)
  print(mean(modelRF$rsq))
  print(mean(modelRF$mse))
  
  
  #### Calculate test R-square on left out year ####
  print(paste("The test year is", list_years_train[[loyo]]))
  test <- data_train_spat_time %>% filter(year == list_years_train[[loyo]])
  # test <- data_train_spat_time %>% filter(year == 2003)
  
  
  #### Make predictions with that trained model ####
  predictions <-  modelRF_loyo %>% predict(test)
  print(predictions)
  
  
  Test_post <- postResample(predictions, test$yield_percentChange)
  print(Test_post)
  Train_Rsquared <- mean(modelRF_loyo$rsq)
  Train_RMSE <- sqrt(mean(modelRF_loyo$mse))
  
  metrics <- c( "test" = Test_post, "train.Rsquared" = Train_Rsquared, "train.RMSE" = Train_RMSE)
  
  print(metrics)
  
  return(metrics)  
  
}